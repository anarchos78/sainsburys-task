# -*- coding: utf-8 -*-

"""
.. module:: .csv.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

import csv
import logging

logger = logging.getLogger(__name__)
handler = logging.StreamHandler()
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)
logger = logging.getLogger('csv')


class CustomCSVParser(object):
    """
    Custom CSV parser.
    """

    def __init__(self, filename, mode='r'):
        """The constructor

        Parameters
        ----------
        filename : str
            File to be processed.
        mode : str : default value 'r'
            The mode in which the file is opened.
            Read mode allowed only.
        """
        self.filename = filename
        self.mode = mode
        self.weekdays = ['mon', 'tue', 'wed', 'thu', 'fri']

    def __enter__(self):
        """Handles the context entry"""
        if self.mode == 'r':
            self.open_file = open(self.filename, self.mode)
        else:
            logger.info("Only read mode allowed on {}".format(self.filename))
        return self

    def __exit__(self, *args):
        """Handles the context exit"""
        self.open_file.close()

    @property
    def file_name(self):
        """Returns the filename stripped from the path"""
        if '/' in self.filename:
            return self.filename.rsplit('/')[-1]
        else:
            return self.filename

    @staticmethod
    def square(value):
        """Returns the square of a value"""
        return int(value) ** 2

    @staticmethod
    def double(value):
        """Returns a value multiplied x2"""
        return int(value) * 2

    @staticmethod
    def format_output(day, description, field, field_value, value):
        """Returns a dictionary with the parsing values"""
        day_data = {
            'day': day,
            'description': '{} {}'.format(description, field_value),
            field: field_value,
            'value': int(value)
        }

        return day_data

    def get_row_data(self):
        """
        Returns the data row of the csv file
        """
        reader = list(csv.DictReader(self.open_file))
        if len(reader) > 1:
            logger.warning(
                "More than 1 data row found. "
                "A different logic needs to be applied!"
            )
        else:
            for row in reader:
                yield row

    def expand_day_range(self, start_day, end_day, value):
        """
        Returns a list. Each element is a 2 element list
        that holds the day and its value, in the case of date range
        e.g. ['mon', 'wed'] ==> [['mon', '2'], ['tue', '2'], ['wed', '2']]
        """
        return [
            [i, value]
            for i
            in self.weekdays[start_day: end_day + 1]
        ]

    def parse(self):
        """
        Returns a list. Each element is a dictionary that holds the parsed data
        """
        week_board, days = [], []

        for item in self.get_row_data():
            try:
                description = item.pop('description')

                for fieldname, value in item.items():
                    if '-' in fieldname:
                        start_end = fieldname.split('-')
                        start_day = self.weekdays.index(start_end[0])
                        end_day = self.weekdays.index(start_end[1])

                        day_range = self.expand_day_range(
                            start_day, end_day, value
                        )

                        days.extend(day_range)
                    elif fieldname not in self.weekdays:
                        continue
                    else:
                        days.append([fieldname, value])

                days.sort(key=lambda x: self.weekdays.index(x[0]))

                for day, value in days:
                    if day in self.weekdays[0:3]:
                        week_board.append(
                            self.format_output(
                                day, description, 'square',
                                self.square(value), value
                            )
                        )
                    else:
                        week_board.append(
                            self.format_output(
                                day, description, 'double',
                                self.double(value), value
                            )
                        )

                return week_board
            except KeyError as e:
                logger.warning(
                    "Field {} not found.".format(e)
                )

                return False
