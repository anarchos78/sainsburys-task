# -*- coding: utf-8 -*-

"""
.. module:: .main.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

from __future__ import print_function

import os
import glob

from pprint import pprint

from parser.csv_parser import CustomCSVParser


if __name__ == '__main__':
    csv_dir = os.path.abspath(
            os.path.join(
                os.path.dirname(__file__), 'csv_files'
            )
        )

    csv_files = sorted(
        glob.glob('{}/*.csv'.format(csv_dir)), key=lambda x: x.rsplit('/')[-1]
    )

    for csv_file in csv_files:
        with CustomCSVParser(csv_file) as fh:
            print(fh.file_name)
            pprint(fh.parse())
            print()
