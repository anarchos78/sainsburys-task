# Custom CSV parser

The csv parser analyze the content of specific structure csv files and produces a specific output.
It parses weekdays related data.

The parsing rules are:
- For each day you should store the fields and values of `day`, `description`, `field` and `value`.
- For mon, tue and wed it is a `square` field.
- For thu, fri it is a `double` field.
- The `description` field contains day specific data.
- Data not related to the aforementioned fields should be skipped.

Notes: days may be provided in a range format e.g. `mon-thu`.

For example, a csv file named `2.csv` and with content:
```sh
mon-thu,fri,description,another_column2
2,3,second_desc,some_data
```

should produce the following output:
```sh
2.csv
[{'day': 'mon', 'description': 'second_desc 4', 'square': 4, 'value': 2},
 {'day': 'tue', 'description': 'second_desc 4', 'square': 4, 'value': 2},
 {'day': 'wed', 'description': 'second_desc 4', 'square': 4, 'value': 2},
 {'day': 'thu', 'description': 'second_desc 4', 'double': 4, 'value': 2},
 {'day': 'fri', 'description': 'second_desc 6', 'double': 6, 'value': 3}]
```

Also, the parser respects the day order.


#### Instructions
Open a terminal and:

1. Clone the repo `git clone https://bitbucket.org/anarchos78/sainsburys-task.git`.
2. Go into the application directory `cd sainsburys-task`.
3. Create a virtual environment for the project, using python 2.7.x or 3.6.x and activate it (I prefer pyenv). The parser works the same on Python 2.7.x and Python 3.6.x
4. To run the tests in the project's directory issue the command `python -m unittest discover`.
5. To run the parser issue `python main.py`.

Keep in mind that the parser uses the built-in modules, so, no dependencies.
