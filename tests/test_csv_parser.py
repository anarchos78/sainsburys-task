# -*- coding: utf-8 -*-

"""
.. module:: .test_csv_parser.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

import os
import types
import unittest

from parser.csv_parser import CustomCSVParser


class TestCustomCSVParser(unittest.TestCase):
    """
    CustomCSVParser test case
    """

    def setUp(self):
        """Test case setup"""

        self.test_filename = 'test.csv'
        self.csv_file_path = os.path.join(
                os.path.dirname(__file__), self.test_filename
            )
        self.ccp = CustomCSVParser(self.csv_file_path)
        self.parser = self.ccp.__enter__()

    def tearDown(self):
        """Clean up"""
        self.parser.__exit__()

    def test_file_name(self):
        """Test file_name property"""
        self.assertEqual(self.parser.file_name, self.test_filename)

    def test_square(self):
        """Test square method"""
        self.assertEqual(self.parser.square('6'), 36)

    def test_double(self):
        """Test double method"""
        self.assertEqual(self.parser.double('6'), 12)

    def test_format_output(self):
        """Test format_output method"""
        formatted_output = self.parser.format_output(
            day='mon', description='mon_desc', field='square',
            field_value=self.parser.square('6'), value='6'
        )
        self.assertEqual(
            formatted_output,
            {
                'day': 'mon',
                'description': 'mon_desc 36',
                'square': 36,
                'value': 6
            }
        )

    def test_get_row_data(self):
        """Test get_row_data method"""
        row = list(self.parser.get_row_data())

        self.assertIsInstance(self.parser.get_row_data(), types.GeneratorType)
        self.assertIsInstance(row[0], dict)

        self.assertIn('mon', str(row[0]))
        self.assertIn('some_column1', str(row[0]))
        self.assertIn('description', str(row[0]))

    def test_expand_day_range(self):
        """Test expand_day_range method"""
        expand_day_range = self.parser.expand_day_range(0, 2, '2')
        self.assertEqual(
            expand_day_range,
            [['mon', '2'], ['tue', '2'], ['wed', '2']]
        )

    def test_parse(self):
        """Test parse method"""
        week_board = self.parser.parse()

        self.assertIsInstance(week_board, list)
        self.assertIsInstance(week_board[0], dict)

        self.assertIn('mon', str(week_board))
        self.assertIn('fri', str(week_board))
        self.assertIn('description', str(week_board))
        self.assertIn('first_desc 25', str(week_board))
